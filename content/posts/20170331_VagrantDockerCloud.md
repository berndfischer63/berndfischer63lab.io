+++
title     = "Vagant based Docker-Swarm"
draft     = true
date      = "2017-03-31T21:51:52+02:00"
highlight = "true"
index     = "true"
tags      = [ "Docker", "Docker-Machine", "Vagrant" ]
+++

Creating Docker-Hosts with Docker-Machine works like a charm. There are great magic behind it - and I'm curious how it works.

And to not keep things that easy we wont create a single Docker-Host only. Instead we will create a Docker-Swarm.

This was the begin of an interesting experiment ...

#### Introduction

Because we used Vagrant and Ansible for installing and configuring our application with great success we gave them a try. Unfortunately Ansible doesn't play well on Windows - therefore rest of the post targets Mac OS X and should work on Linux too (but not yet tested). Sorry to all Windows users out there!

To complete our crew we chose Virtualbox to create our local virtual machine.
