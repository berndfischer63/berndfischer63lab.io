+++
date        = "2015-02-13T13:56:00+01:00"
draft       = "false"
title       = "First Experience with Named Dockerfiles"
description = "description"
highlight   = "true"
index       = "true"
tags        = [ "Cloud Computing", "Container", "Docker" ]
+++

On Februar 10th Docker Inc. announced the general availability of Docker 1.5. It includes one of the most requested features in the past few months: The ability to specify the file to use in a `docker build`.

As soon I had some spare time I updated my Docker environment (Mac OS X 10.10 with boot2docker) to be able to try it. In my scenario it had to be done in two steps:

#### Part1: Updating Boot2Docker

Because Boot2Docker is divided into the “core” located on a read only partition based on boot2docker.iso and a “writable user data” partition (usually named “boot2docker-vm” but can be changed) updating the “core” without touching user data is an easy task. The boot2docker.iso has to be replaced with a newer one.

You can achieve that with following commands

```bash
boot2docker stop
boot2docker download
boot2docker up
```

Thats all ;-)

#### Part2: Updating Docker client

To interact with the Docker daemon running in an VM provided by Boot2Docker you usually use the Docker Client (CLI). In case of Docker daemon (server) and client functionality is provided by same executable.

Depending on the way how you installed Docker on your computer several possibilities for the update exists. If you are using [Homebrew](http://brew.sh/) like me than issue following commands

```bash
brew update
brew upgrade docker
```

should do it. Afterwards you should be done.

Remark: First command (`brew update`) ensures that Brew uses current `Formulae’s`. Take it as preparation for updating Docker itself.

Now we are prepared to go on with real stuff.

#### Part3: Docker build

Let us assume a directory “example_dir” exists somewhere on our computer. The concrete path of it should be stored in environment variable `EXAMPLE_DIR`, so we can change the current directory with

```bash
cd $EXAMPLE_DIR
```

This directory should contain a subdirectory `jenkins` which contains an untypical Dockerfile `jenkins.df`. It is *untypical* because until now a Dockerfile has to be named `Dockerfile` and nothing else …

```bash
ls -al
drwxr-xr-x   8 bf  staff   272 Feb  6 12:12 .
drwxr-xr-x   5 bf  staff   170 Feb 11 21:18 ..
drwxr-xr-x   4 bf  staff   136 Feb 11 21:16 jenkins

ls -al ./jenkins
drwxr-xr-x  3 bf  staff  102 Feb 11 21:17 .
drwxr-xr-x  8 bf  staff  272 Feb  6 12:12 ..
-rw-r--r--  1 bf  staff  525 Feb  6 20:11 jenkins.df
```

The content of `jenkins.df` doesn’t matter for our purpose. But it should work ;-) And remember our working directory is still `$EXAMPLE_DIR` and not `$EXAMPLE_DIR/jenkins`.

To **enable** new functionality for Docker build we have to provide additional parameter `-f filename`

```bash
docker build -f ./jenkins/jenkins.df .
```

This works but not like we expect. Because the build of a Docker image is now influenced by two parameters

* the Dockerfile and
* the Dockercontext

Dockercontext controls which “files” are send to the Docker daemon (this are the local files you can use in Dockerfile with commands like ADD or COPY). If, like in our scenario, the directory defining the root of the Dockercontext (btw, this is done by the last sign of the docker build command: the period), is empty the impact will be less. But imagine it could contain more and bigger files …

Therefore we should improve the aforementioned build command to

```bash
docker build -f ./jenkins/jenkins.df ./jenkins
```

At last a more general variant

```bash
docker build -f <docker-file> <docker-context-root-dir>
```

#### Part4: - Conclusion

This extension of Docker build command allows to store more than one Dockerfile in one directory and separates them from location of Dockercontext. With other words: It increases flexibility how to layout the directory structure of projects using Docker.

This is a real improvement and makes working with  Docker more fun.
