+++
date        = "2015-02-16T13:56:00+01:00"
draft       = "false"
title       = "Continuous Delivery and Jenkins Workflow Engine"
description = "description"
highlight   = "true"
index       = "true"
tags        = [ "Jenkins", "Build Pipeline", "Continuous Delivery", "DevOps" ]
+++

Trying to implement a Continuous Delivery Pipeline according to the well known book from Jez Humble and David Farley [01] with Jenkins as CI (Continuous Integration) server, everyone stumbled over following plugins and “technics”

* Build Pipeline Plugin [06]
* Build Flow Plugin [07]
* Delivery Pipeline Plugin [08]
* Parametrised Trigger Plugin [09]
* Job Chaining (up-/downstream)

Possibly you have needed some more.

It was never an easy task whether you used a particular plugin only or a set of them. Every variant of building the pipeline had and has its own pros and cons for sure.

Last year CloudBees Inc. (the company behind Jenkins) announced the “Workflow Engine” [02][03] which should be able to replace all the aforementioned plugins and should make the usage of  Jenkins much more developer friendly.

The announcements sound promising, so I decided to give it a try. I would like to invite every one who is interested in this topic to follow my journey in the upcoming blog posts.

#### References and interesting Links

[01] Jez Humble, David Farley: Continuous Delivery. Addison-Wesley - Copyright 2011 Pearson Education inc. (e.g. via [Amazon](http://www.amazon.de/Continuous-Delivery-Deployment-Automation-Addison-Wesley/dp/0321601912/))</br>
[02] [Jenkins Workflow Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Workflow+Plugin)</br>
[03] [Jenkins Workflow Plugin - GitHub-Repo](https://github.com/jenkinsci/workflow-plugin)</br>
[04] [Jenkins Workflow Screencast](https://www.youtube.com/watch?v=Welwf1wTU-w)</br>
[05] [Webinar Orchestrating the CD Process in Jenkins with Workflow](http://blog.cloudbees.com/2014/12/webinar-q-orchestrating-cd-process-in.html)</br>
[06] [Jenkins Pipeline Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Build+Pipeline+Plugin)</br>
[07] [Jenkins Build Flow Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Build+Flow+Plugin)</br>
[08] [Jenkins Delivery Pipeline Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Delivery+Pipeline+Plugin)</br>
[09] [Jenkins Parametrized Trigger Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Parameterized+Trigger+Plugin)</br>
