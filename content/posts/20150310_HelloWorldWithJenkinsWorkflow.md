+++
date        = "2015-03-10T20:56:00+01:00"
draft       = "false"
title       = "Hello World with Jenkins Workflow Engine"
description = "description"
highlight   = "true"
index       = "true"
tags        = [ "Jenkins", "Build Pipeline", "Continuous Delivery", "DevOps" ]
+++

After successfully creating and running a Docker image/container with Jenkins and the new “Workflow-Engine” we should get our fingers dirty and code a first “workflow” ;-) As usual that would be some kind of “Hello World” …

As first step we create a new Jenkins job of type “Workflow” and name it “HelloWorld”.

<p/>
<figure>
<img src="/img/20150310-10.png" width="650" alt="Create new Jenkins Workflow Job"/>
<figcaption><p><small>Figure 1: Create new Jenkins Workflow Job</small></p></figcaption>
</figure>
<p/>

To keep things simple we don’t use any source code repository (SCM) for this example. So we have to fill in some more or less useful commands (like `echo('Hello World');``) into script area of the Workflow configuration section and check “Use Groovy Sandbox”. Nothing more, nothing less.

<p/>
<figure>
<img src="/img/20150310-11.png" width="650" alt="Workflow Configuration Section"/>
<figcaption><p><small>Figure 2: Workflow Configuration Section</small></p></figcaption>
</figure>
<p/>

After saving the job we have to start it manually by pressing “Build Now” button.

<p/>
<figure>
<img src="/img/20150310-12.png" width="650" alt="Jenkins Job Control Page"/>
<figcaption><p><small>Figure 3: Jenkins Job Control Page</small></p></figcaption>
</figure>
<p/>

If you look at the console output afterwards you should see something similar:

<p/>
<figure>
<img src="/img/20150310-13.png" width="650" alt="Jenkins Job Console Output"/>
<figcaption><p><small>Figure 4: Jenkins Job - Console Output</small></p></figcaption>
</figure>
<p/>

That’s all. We created and run our first (simple) Jenkins Workflow job.

#### Summary

Jenkins Workflow Engine adds a new job type named “Workflow”. In contrast to other job types it doesn’t provide so much configuration possibilities: Of course you can
* give it a name,
* trigger the build by
** scheduling,
** polling a source repository or
** joining to another project and
* write or provide a Groovy based DSL script.

This script is there all the magic happens, usually more than echo-ing "Hello World" - but this we will explore more deeply in following posts.
