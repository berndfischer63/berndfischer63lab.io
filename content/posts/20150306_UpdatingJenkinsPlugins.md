+++
date        = "2015-03-06T11:34:00+01:00"
draft       = "false"
title       = "Updating Jenkins Plugins"
description = "description"
highlight   = "true"
index       = "true"
tags        = [ "Jenkins", "Build Pipeline", "Continuous Delivery", "DevOps" ]
+++

Some days ago (to be more exactly: On March 4, 2015) new version 1.3 of Workflow Engine was released. A good reason to think about how to update our Jenkins installation.

Of course easiest way would be to do it manually using Jenkins Plugin Manager.

<p/>
<figure>
<img src="/img/20150306-01.png" width="650" alt="Jenkins Plugin Manager - Update Sheet"/>
<figcaption><p><small>Figure 1: Jenkins Plugin Manager - Update Sheet</small></p></figcaption>
</figure>
<p/>

Choose "Updates" sheet, check plugins you like to update, press button "Download now and install after restart" and wait some minutes.

We like to refer to it as the "traditionell way" which works pretty well and stable.

But on the other side patterns like "Immutable Server" [01] or "Phoenix Server" [02] get more and more attention. At first look it seems easy to achieve because Jenkins separates itself (the core) from any other stuff, which is located in a special directory usually named "jenkins-home".

In our installation variant based on Docker we delivered plugins as part of the Docker image, but to preserve manual changes Jenkins will use them only if they are new (more precisely: they are copied from `/usr/share/jenkins/ref/plugins` to `/var/jenkins-home/plugins` during start of container but only if they don't exist already). Conclusion: We can add plugins via Docker image but we can't update existing plugins.

The only workaround we found is

1. stopping Jenkins
1. delete content of plugins directory
1. create new Jenkins-Docker image with updated plugins (do you remember configuration file "plugins.txt" from my last post [03]?) and
1. create and run new Jenkins-Docker container
1. if all steps went well delete "old" container (optional but recommended)

Now all plugins inside Jenkins-Docker image are new and will be copied to plugin subdirectory inside the already mentioned "jenkins-home".

#### Summary

Updating Jenkins plugins by providing them in Docker image looks crucial. It needs some improvement before it can be used in production.

#### References and interesting links

[01] [Martin Fowler: PhoenixServer](http://martinfowler.com/bliki/PhoenixServer.html)</br>
[02] [Kief Morris: ImmutableServer](http://martinfowler.com/bliki/ImmutableServer.html)</br>
[03] [Jenkins Docker Image with Workflow Engine](posts/201502_jenkins-docker-image-with-workflow.html)</br>
