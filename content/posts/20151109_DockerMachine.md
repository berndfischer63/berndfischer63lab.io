+++
date        = "2015-11-09T15:38:00+01:00"
draft       = "false"
title       = "First Experience with Docker-Machine 0.5.0"
description = "description"
highlight   = "true"
index       = "true"
tags        = [ "Cloud", "Docker", "Docker-Machine" ]
+++

Last weekend I had some time for "playing" with the brand new versions of Docker-Engine (v1.9) and Docker-Machine (v0.5.0).

First I installed them. Using "brew" on Mac (Mac OS X 10.11.1) this was not a big deal.

```bash
brew update
brew upgrade
```

Next step was to "migrate" my local "Docker-Tool-VM" based on VirtualBox V5.0.8 from Docker v1.8 to current version. Therefore I started it first:

```bash
docker-machine start tools
```

```toml
Checking if hardware virtualization is enabled failed:
open /Users/bf/.docker/machine/machines/tools/tools/Logs/VBox.log: no such file or directory
```

What?
Never got such strange kind of message during months of working with "Docker-Machine"!

Even asking "Google" didn't help ... It seems that no one else went into this trouble :unamused:

After some investigation I found the file "VBox.log" on a little bit different directory than "Docker-Machine" expected it. Instead of

```bash
/Users/bf/.docker/machine/machines/tools/tools/Logs
```

it exists at

```bash
/Users/bf/.docker/machine/machines/tools/DockerMachine/tools/Logs
```

Hm - let's try what will happen if we copy "VBox.log" and all other directories and files from "wrong" to "right" location?

```bash
cp \
  /Users/bf/.docker/machine/machines/tools/DockerMachine/tools \
  /Users/bf/.docker/machine/machines/tools/tools
```

And than it was time to start "Docker-Tool-VM" again.

```bash
docker-machine start tools

Started machines may have new IP addresses. You may need to re-run the `docker-machine env` command.
```

Success!

I still don't know what really happened here - but I'm happy that it works again :-) If someone knows more about background and/or reasons: Feel free to add a comment.
